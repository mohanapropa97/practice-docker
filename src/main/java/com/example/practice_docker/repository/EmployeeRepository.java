package com.example.practice_docker.repository;
import com.example.practice_docker.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findByEmployeeId(String employeeID);

    boolean existsByEmployeeId(String employeeId);

    int countByDesignation(String designation);
 /*   @Query(value = "SELECT * FROM EMPLOYEE ORDER BY AGE ASC", nativeQuery = true)
    List<Employee> findEmployeeByAgeOrderBy();*/

}
