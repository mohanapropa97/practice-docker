package com.example.practice_docker.service;

import com.example.practice_docker.dto.EmployeeDto;
import com.example.practice_docker.entity.Employee;
import com.example.practice_docker.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class EmployeeService {

    public final EmployeeRepository employeeRepository;


    public String saveEmployee(EmployeeDto employeeDto) {
        if (employeeRepository.existsByEmployeeId(employeeDto.getEmployeeId()))
            return "Employee already Exist with this EmployeeID";
        else {
            Employee employee = new Employee();
            BeanUtils.copyProperties(employeeDto, employee);
            employeeRepository.save(employee);
            return "Saved Successfully";
        }
    }

    public List<EmployeeDto> getEmployee() {
        List<Employee> employeeList = employeeRepository.findAll();
        List<EmployeeDto> employeeDtoList = new ArrayList<>();
        for (Employee employee : employeeList) {

            EmployeeDto employeeDto = new EmployeeDto();
            BeanUtils.copyProperties(employee, employeeDto);
            employeeDtoList.add(employeeDto);
        }
        return employeeDtoList;
    }


}
