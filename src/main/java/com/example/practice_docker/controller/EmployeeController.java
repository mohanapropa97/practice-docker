package com.example.practice_docker.controller;

import com.example.practice_docker.dto.EmployeeDto;
import com.example.practice_docker.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
@AllArgsConstructor

public class EmployeeController {

    private final EmployeeService employeeService;

    @PostMapping("/save")
    public ResponseEntity<?> saveEmployee(@RequestBody EmployeeDto employeeDto) {
        String message = employeeService.saveEmployee(employeeDto);
        //return ResponseEntity.ok(message);
        return new ResponseEntity<>(message, HttpStatus.CREATED);
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> getEmployee() {
        List<EmployeeDto> employeeDtoList = employeeService.getEmployee();
        return new ResponseEntity<>(employeeDtoList, HttpStatus.OK);
    }

    

}
