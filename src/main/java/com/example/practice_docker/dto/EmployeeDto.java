package com.example.practice_docker.dto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeDto {
    private long id;
    private String employeeId;
    private String employeeName;
    private String designation;
}
